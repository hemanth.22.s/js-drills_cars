  const problem5 = function (arg){
  if((arg!==undefined) && (arg.length!==0)){
        let carsYear = []
        for(let i = 0; i<arg.length; i++){
            if(arg[i].car_year<2000){
                carsYear.push(arg[i])
            }
        }
    
        return carsYear
    }
    else{
        return []
    }
}

module.exports = problem5;

