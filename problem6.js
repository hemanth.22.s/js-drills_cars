const problem6 =  function (arg){
    if((arg!==undefined) && (arg.length!==0)){
        let filteredCars = []
        for(let i=0; i<arg.length; i++){
            if((arg[i].car_make==='BMW') || (arg[i].car_make==='Audi')){
                filteredCars.push(arg[i])
            }
        }
        return filteredCars;
    }
    else{
        return []
    }
    
}
module.exports = problem6;