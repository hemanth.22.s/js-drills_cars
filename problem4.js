const problem4 = function(arg){
    if((arg!==undefined) && (arg.length!==0)){
        let yearsCars = []
        for (let i = 0; i < arg.length; i++){
            yearsCars.push(arg[i].car_year)
        }
        return yearsCars
    }
    else {
        return []
    }
}

module.exports = problem4;

